# -*- coding: utf-8 -*-

"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

"""
Implémentation de différents décorateurs python génériques
"""


def singleton(singleclass):
    """
    Implémentation python du décorateur singleton
    N'autorise qu'une seule instance pour un objet donné
    
    SYNTAXE :
    
    @singleton
    class maClass(...):
        ....

    ou

    maClass = singleton(maClass)

    """
    singles = {}
    def get_singleton():
        if singleclass not in singles:
            singles[singleclass] = singleclass()
        return singles[singleclass]
    return get_singleton


# http://www.siteduzero.com/tutoriel-3-323958-les-decorateurs.html
def types(*a_args, **a_kwargs):
    """On attend en paramètre du décorateur les types attendus. On accepte
    une liste de paramètres indéterminés, étant donné que notre fonction
    définie pourra être appelée avec un nombre variable de paramètres, et que
    chacun doit être contrôlé.
    
    """
    def decorate(function):
        def make_function(*args, **kwargs):
            """Notre fonction modifiée. Elle se charge de contrôler
            les types qu'on lui passe en paramètre.
            
            """
            # la liste des paramètres attendus (a_args) doit être de même
            # longueur que celle reçue (args)
            if len(a_args) != len(args):
                raise TypeError("le nombre d'arguments attendu n'est pas égal " \
                        "au nombre reçu")
            # on parcourt la liste des arguments reçus et non nommés
            for i, arg in enumerate(args):
                if a_args[i] is not type(args[i]):
                    raise TypeError("l'argument {0} n'est pas du type " \
                            "{1}".format(i, a_args[i]))
            
            # on parcourt à présent la liste des paramètres reçus et nommés
            for cle in kwargs:
                if cle not in a_kwargs:
                    raise TypeError("l'argument {0} n'a aucun type " \
                            "précisé".format(repr(cle)))
                if a_kwargs[cle] is not type(kwargs[cle]):
                    raise TypeError("l'argument {0} n'est pas de type" \
                            "{1}".format(repr(cle), a_kwargs[cle]))
            return function(*args, **kwargs)
        return make_function
    return decorate
