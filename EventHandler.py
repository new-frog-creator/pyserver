# -*- coding: utf-8 -*- 

"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

"""
Représentation du serveur a partir des messages reçu par le réseau
Voir aussi Server.py
"""

import traceback
import sys
import Protocol
import Config

from Modules.Import import *

class EventHandler(object):

    
    def __init__(self, env):
        """
        Initialise le module avec les objets de l'environnement env
        """

        self._env = env

    def onConnexion(self,user):
        """
        Appelé à la connexion d'un client,
        Déclenche les scripts décoré avec la fonction connect de Protocol.Library
        """
        print ('connection de %s ' % user.ip )
    
    def onDisjunction(self,user):
        """
        Appelé à la deconnexion d'un client,
        Déclenche les scripts décoré avec la fonction disconnect de Protocol.Library
        """
        if user.logged:
            user.logged = False
        if user.user_entry:
            user.user_entry.connected = False
        print ('deconnection de %s ' % user.ip)

    def onMessage(self,user,message):
        """
        Appelé lorsqu'un client envoit un message (au sens du protocole, et non in-game)
        """
        print ('message recu de %s : %s' % (user.ip, message))
        tag = message.split(self._env.config.SEP)[0].lower()
         
        orders = Protocol.Library().getOrders()
        if not tag in orders:
            print ('[fixme] tag "%s" in message = "%s"' % (tag,message)) 
            print ('tags : %s ' % orders)
            return 

        for order in orders[tag]:
            print ('Use : %s' % order) 
            try:
                order(self._env, user, message)
            except Exception as e:
                print ('EXCEPTION : %s' % e)
                traceback.print_exc(file=sys.stdout)
            
