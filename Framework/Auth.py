# -*- coding: utf-8 -*- 
"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Fonctionnalitées d'authentification pour le serveur

AUTHOR  : Janniaux Alexandre
"""

def IsNameValid(name, charset):
    """
    Vérifie la validité d'un nom
    
    il est conseillé de mettre les caractères autorisés (le charset) dans le fichier config
    """

    charset = tuple(charset)
    t = tuple(name)
    for k in t:
        equality = False
        for c in charset:
            if c==k:
    	        equality = True
            	break

        if equality == False:
            return False
    return True


def SetPassword(user,password):
    """
    Change le mot de passe du joueur

    Utiliser cette fonction pour pouvoir changer de méthode de cryptage
    """
    user.password = password


def IsValidPassword(player,password):
    """
    Vérifie que le joueur a bien le bon mot de passe

    Utilisez cette fonction pour pouvoir changer de méthode de cryptage
    """
    return str(player["password"]) == str(password) # TODO: use types decorator

