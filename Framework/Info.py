"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

import Packet

from Framework import Update
from GameData import Class, Item, Pet, Emoticon, Arrow, Npc, Shop, Spell

def ClassesMsg(env):
    classes = Class.Class.select()
    count = classes.count()
        
    p = Packet.Packet()
    p << 'CLASSESDATA' 
    if (count==0):
        p << 1
        p << 'Pas de classe' << 0 << 0 << 0 <<0 << 0 << 0 << 1
    else: 
        p << count-1
        for c in classes:
            p << c.name
            p << c.maxHP
            p << c.maxMP
            p << c.maxSP
            p << c.STR
            p << c.DEF
            p << c.speed
            p << c.magie
            p << int(c.locked)
    return p
 
def ItemsMsg(env):
    items = Item.Item.select()
    if items.count() > 0:
        return [Update.ItemMsg(env, item) for item in items]
    return None
    
def PetsMsg(env):
    pets = Pet.Pet.select()
    if pets.count() > 0:
        return tuple([Update.PetMsg(env, pet) for pet in pets])
    return None
    
def EmoticonsMsg(env):
    emoticons = Emoticon.Emoticon.select()
    if emoticons.count() > 0:
        return tuple([Update.EmoticonMsg(env, emoticon) for emoticon in emoticons])
    return None

def ArrowsMsg(env):
    arrows = Arrow.Arrow.select()
    if arrows.count() > 0:
        return tuple([Update.ArrowsMsg(env,arrow) for arrow in arrows])
    return None
    
def NpcsMsg(env):
    npcs = Npc.Npc.select()
    if npcs.count() > 0:
        return tuple([Update.NpcMsg(env, npc) for npc in npcs])
    return None
    
def ShopsMsg(env):
    shops = Shop.Shop.select()
    if shops.count() > 0:
        return tuple([Update.ShopMsg(env, shop) for shop in shops])
    return None

def SpellsMsg(env):
    spells = Spell.Spell.select()
    if spells.count() > 0:
        return tuple([Update.SpellMsg(env, spell) for spell in spells])
    return None
    
def WeatherMsg(env):
    p = Packet.Packet()
    # TODO : WEATHER gameweather rainintensity
    # 0 : soleil 
    # 1 : pluie
    # 2 : Orage
    p << 'WEATHER' << 0 << 1 
    return p
    
def TimeMsg(env):
    p = Packet.Packer()
    p << 'TIME' 
    
