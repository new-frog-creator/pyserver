# -*- coding:utf-8 -*-
"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


Message est un framework qui permet de raccourcir les scripts via des éléments communs et prédéfinis
"""

import Config
import Packet
from GameData import Class, Character

def PlainMsg(message,num):
    p = Packet.Packet()
    p << 'PLAINMSG' << message << str(num) 
    return p

def GlobalMsg(message,color):
    p = Packet.Packet()
    p << 'GLOBALMSG' << message << str(color)
    return p


def MaxInfo(config):
    p = Packet.Packet()
    p << 'MAXINFO' 
    p << str(config.GAME_NAME)
    p << str(config.MAX_PLAYERS)
    p << str(config.MAX_ITEMS)      
    p << str(config.MAX_NPCS)   
    p << str(config.MAX_SHOP)     
    p << str(config.MAX_SPELLS)     
    p << str(config.MAX_MAPS)    
    p << str(config.MAX_MAP_ITEMS)    
    p << str(config.MAX_MAPX)    
    p << str(config.MAX_MAPY)    
    p << str(config.MAX_EMOTICONS)    
    p << str(config.MAX_LEVEL)    
    p << str(config.MAX_QUETES)    
    p << str(config.MAX_INV)    
    p << str(config.MAX_NPC_SPELLS)
    p << str(config.MAX_PETS)
    return p

def CharListMsg (player, config):
    p = Packet.Packet()

    p << 'ALLCHARS' 
    chars = Character.Character.selectBy(user=player)
    playertotal = chars.count()
    if playertotal > config.MAX_CHARS:
        chars = chars[0:config.MAX_CHARS-1]
        playertotal = config.MAX_CHARS

    playerleast = config.MAX_CHARS - playertotal
    print ('%s chars, %s emplacements libres' % (playertotal, playerleast))
    for char in chars:
        p << char.name << char.class_type.name << char.level    
        print ('add a char %s class %s level %s' % (char.name, char.class_type,char.level))
    for i in range(playerleast):
        p << ''
        p << ''	
        p << ''	

    return p

