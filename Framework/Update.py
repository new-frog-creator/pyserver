"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

import Packet

def ItemMsg(env, item):
    p = Packet.Packet()
    p << 'UPDATEITEM' 
    p << item.id
    p << item.name
    p << item.pic
    p << item.type
    p << item.data1
    p << item.data2
    p << item.data3
    p << item.STR_req
    p << item.DEF_req
    p << item.speed_req
    p << item.class_req
    p << item.access_req
    
    p << item.HP_add
    p << item.MP_add
    p << item.SP_add
    p << item.STR_add
    p << item.DEF_add
    p << item.magie_add
    p << item.speed_add
    p << item.exp_add
    p << item.description
    p << item.atkspeed_add
    p << item.num_coul
    p << int(item.paperdoll)
    p << item.paperdoll_pic
    p << int(item.stackable)    
    return p
    
def PetMsg(env, pet):
    p = Packet.Packet()
    p << 'UPDATEPET'
    p << pet.id
    p << pet.name
    p << pet.sprite
    p << pet.STR_add
    p << pet.DEF_add
    return p
    
def EmoticonMsg(env, emoticon):
    p = Packet.Packet()
    p << 'UPDATEEMOTICON'
    p << emoticon.id
    p << emoticon.command
    p << emoticon.pic
    return p
    
    
def ArrowMsg(env, arrow):
    p = Packet.Packet()
    p << 'UPDATEARROW'
    p << arrow.id
    p << arrow.name
    p << arrow.pic
    p << arrow.range
    return p
    
def NpcMsg(npc):
    p = Packet.Packet()
    p << 'UPDATENPC'
    p << npc.id
    p << npc.name
    p << npc.sprite
    p << npc.maxHP
    p << npc.quete_num
    p << npc.behavior
    p << npc.inv
    p << npc.vol
    return p
    
def ShopMsg(env, shop):
    p = Packet.Packet()
    p << 'UPDATESHOP'
    p << shop.id
    p << shop.name
    return p
    
def SpellMsg(env, spell):
    p = Packet.Packet()
    p << 'UPDATESPELL'
    p << spell.id 
    p << spell.name
    p << spell.big
    p << spell.icone
    return p
    
def QueteMsg(env, quete):
    p = Packet.Packet()
    p << 'UPDATEQUETE'
    p << quest.id
    p << quest.name
    p << quest.data1
    p << quest.data2
    p << quest.data3
    p << quest.description
    p << quest.response
    p << quest.string1
    p << quest.time
    p << quest.quest_type    
    return p
    
