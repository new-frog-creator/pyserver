# -*- coding: utf-8 -*- 

"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

"""
Exécutable du serveur 
"""

import Server
import Protocol
import Env
import EventHandler
import Config

import threading
import os
import sys

from sqlobject import *
from GameData import User, Class, Character, Item, Npc, Pet, Emoticon, Arrow, Shop, Spell, Map


from Tools import AutoCreateClass

if __name__ == '__main__':
    config1         = __import__('conf1')
    serv            = Server.Server(config1.ADDRESS,config1.PORT)
    env             = Env.Env()

    db_filename = os.path.abspath('data.db')
#    if os.path.exists(db_filename):
#        os.unlink(db_filename)
    connection_string = 'sqlite:' + db_filename
    connection = connectionForURI(connection_string)
    sqlhub.processConnection = connection
    
    User.User.createTable(ifNotExists=True)
    Class.Class.createTable(ifNotExists=True)
    Character.Character.createTable(ifNotExists=True)
    Item.Item.createTable(ifNotExists=True)
    Npc.Npc.createTable(ifNotExists=True)
    Pet.Pet.createTable(ifNotExists=True)
    Emoticon.Emoticon.createTable(ifNotExists=True)
    Arrow.Arrow.createTable(ifNotExists=True)
    Shop.Shop.createTable(ifNotExists=True)
    Spell.Spell.createTable(ifNotExists=True)
    Map.Map.createTable(ifNotExists=True)
    
    AutoCreateClass.AutoCreateClass()

    env.server      = serv
    env.config      = config1

    eventhandler    = EventHandler.EventHandler(env)
    serv.onConnexion.connect(eventhandler.onConnexion)
    serv.onMessage.connect(eventhandler.onMessage)
    serv.onDisjunction.connect(eventhandler.onDisjunction)
    serv.start()
    print ('Server started !')
    while True:
        serv.loop()
