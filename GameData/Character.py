"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

from sqlobject import *
from GameData import User, Class, Guild

class Character(SQLObject):
    user = ForeignKey('User')
    name = UnicodeCol(default='')
    class_type = ForeignKey('Class', notNull=False, default=None)
    sex = IntCol(default=0) #EnumCol() ?
    sprite = IntCol(default=0)
    level = IntCol(default=1)
    experience = IntCol(default=0)
    access = IntCol(default=0)
    pk = BoolCol(default=False)
    guild = ForeignKey('Guild', notNull=False, default=None)
    guild_acces = IntCol(default=0)
    HP = IntCol(default=0)
    MP = IntCol(default=0)
    SP = IntCol(default=0)
    STR = IntCol(default=0)
    DEF = IntCol(default=0)
    speed = IntCol(default=0)
    magie = IntCol(default=0)
    points = IntCol(default=0)
    has_pet = BoolCol(default=False) # need?
    game_map = IntCol(default=0)
    x = IntCol(default=0)
    y = IntCol(default=0)
    direction = IntCol(default=0) #EnumCol() ?
    
    weapon = ForeignKey('Item', default=None)
    armor = ForeignKey('Item', default=None)
    shield = ForeignKey('Item', default=None)
    helmet = ForeignKey('Item', default=None)
    
