"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

from sqlobject import *

class Item(SQLObject):
    name = UnicodeCol(default='')
    description = UnicodeCol(default='')
    pic = IntCol(default=0)
    type = IntCol(default=0)
    data1 = IntCol(default=0)
    data2 = IntCol(default=0)
    data3 = IntCol(default=0)
    STR_req = IntCol(default=0)
    DEF_req = IntCol(default=0)
    speed_req = IntCol(default=0)
    class_req = IntCol(default=0)
    access_req = BoolCol(default=False)
    paperdoll = BoolCol(default=False)
    paperdoll_pic = IntCol(default=0)
    stackable = BoolCol(default=False)
    HP_add = IntCol(default=0)
    MP_add = IntCol(default=0)
    SP_add = IntCol(default=0)
    STR_add = IntCol(default=0)
    DEF_add = IntCol(default=0)
    magie_add = IntCol(default=0)
    speed_add = IntCol(default=0)
    exp_add = IntCol(default=0)
    atkspeed_add = IntCol(default=0)
    num_coul = IntCol(default=0)
    sex = BoolCol(default=0)
    
