from sqlobject import *

class Map(SQLObject):
    
    tiles = []
    npcs = [] # no NPC yet
    name = StringCol(default='')
    revision = IntCol(default=0)
    moral = IntCol(default=0)
    map_up = ForeignKey('Map',default=None,notNull=False)
    map_down = ForeignKey('Map',default=None,notNull=False)
    map_left = ForeignKey('Map',default=None,notNull=False)
    map_right = ForeignKey('Map',default=None,notNull=False)
    music = StringCol(default=0)
    boot_map = IntCol(default=0)
    boot_x = IntCol(default=0)
    boot_y = IntCol(default=0)
    indoors = IntCol(default=0)
    pano_inf = ''
    tran_inf = ''
    pano_sup = ''
    tran_sup = ''
    fog = BoolCol(default=False)
    fog_alpha = IntCol(default=255)
    sizex = IntCol(default=32)
    sizey = IntCol(default=32)
    
    
    def read(self, filename):
        self.tiles = []
        with open(filename, 'rb') as map_file:
            unpick = picke.Unpickler(map_file)
            for i in range(self.sizex*self.sizey):
                tiles.append(unpick.load())
        
    
    def write(self, filename):
        
        with open(filename, 'wb') as map_file:
            pick = picke.Pickler(map_file)
            for i in range(self.sizex*self.sizey):
                if i >= len(self.tiles):
                    pick.dump( Tile.Tile() )
                else:
                    pick.dump( self.tiles[i] )
        
