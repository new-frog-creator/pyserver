#-*- encoding:utf-8 -*-
"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

from sqlobject import *

class Npc(SQLObject):
    name = UnicodeCol(default='')
    attack_say = UnicodeCol(default='')
    sprite = IntCol(default=0)

    spawn_secs = IntCol(default=0)
    behavior = IntCol(default=0)
    spot_range = IntCol(default=0)
    
    STR = IntCol(default=0)
    DEF = IntCol(default=0)
    speed = IntCol(default=0)
    magie = IntCol(default=0)
    maxHP = IntCol(default=0)
    exp = IntCol(default=0)
    spawn_time = IntCol(default=0)
    
#    ItemNPC(1 To MAX_NPC_DROPS) As NPCEditorRec
    quete_num = IntCol(default=0)
    inv = IntCol(default=0)
    vol = IntCol(default=0)
#    Spell(1 To MAX_NPC_SPELLS) As Integer
