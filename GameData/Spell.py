"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

from sqlobject import *
from GameData import Class

class Spell (SQLObject):
    name = UnicodeCol(default='')
    class_req = ForeignKey('Class')
    level_req = IntCol(default=0)
    MP_cost = IntCol(default=0)
    sound = IntCol(default=0)
    spell_type = IntCol(default=0)
    data1 = IntCol(default=0)
    data2 = IntCol(default=0)
    data3 = IntCol(default=0)
    spell_range = IntCol(default=0)
    
    big = IntCol(default=0)
    
    spell_anim = IntCol(default=0)
    spell_time = IntCol(default=0)
    spell_done = IntCol(default=0)
    
    icone = IntCol(default=0)
    
    AE = IntCol(default=0)
