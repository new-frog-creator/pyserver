
class MapManager(object):
    maps = {}
    
    def get_map(self, num):
        if num in self.maps:
            return self.maps[num]
        else:
            try:
                new_map = Map.Map.get(num)
                self.maps[num] = new_map
                self.maps[num].read('maps/%s.map' % (num))
            except:
                print ('inexistant map')