import Packet

def Stats(env, char):
    p = Packet.Packet()
    p << 'PLAYERSTATSPACKET'
    p << char.STR 
    p << char.DEF
    p << char.speed
    p << char.magie
    p << env.logic.xp_left(char.level, char.exp)
    p << char.exp
    p << char.level
    
def MaxHP(env, char):
    armor_HP_add = 0
    if char.weapon: armor_HP_add += char.weapon.HP_add
    if char.armor: armor_HP_add  += char.armor.HP_add
    if char.shield: armor_HP_add += char.shield.HP_add
    if char.helmet: armor_HP_add += char.helmet.HP_add
    return char.level*env.player_stats.HP_add.level + 
            player.STR*env.char_stats.HP_add.STR +
            char.DEF*env.char_stats.HP_add.DEF + 
            char.magie*env.char_stats.HP_add.magie) + 
            char.speed*env.char_stats.HP_add.speed +
            armor_HP_add

def HPMsg(env, char):
    p = Packet.Packet()
    p << 'PLAYERHP' << char.HP << MaxHP(env, char) 
    return p        

def MaxMP(env, char):
    armor_MP_add = 0
    if char.weapon: armor_MP_add += char.weapon.MP_add
    if char.armor: armor_MP_add  += char.armor.MP_add
    if char.shield: armor_MP_add += char.shield.MP_add
    if char.helmet: armor_MP_add += char.helmet.MP_add
    return char.level*env.player_stats.MP_add.level + 
            player.STR*env.char_stats.MP_add.STR +
            char.DEF*env.char_stats.MP_add.DEF + 
            char.magie*env.char_stats.MP_add.magie) + 
            char.speed*env.char_stats.MP_add.speed +
            armor_MP_add   
    
def MPMsg(env, char):
    p = Packet.Packet()
    p << 'PLAYERSP' << char.SP << MaxSP(env, char)
    return p
    
def MaxSP(env, char):
    armor_SP_add = 0
    if char.weapon: armor_SP_add += char.weapon.SP_add
    if char.armor: armor_SP_add  += char.armor.SP_add
    if char.shield: armor_SP_add += char.shield.SP_add
    if char.helmet: armor_SP_add += char.helmet.SP_add
    return char.level*env.player_stats.SP_add.level + 
            player.STR*env.char_stats.SP_add.STR +
            char.DEF*env.char_stats.SP_add.DEF + 
            char.magie*env.char_stats.SP_add.magie) + 
            char.speed*env.char_stats.SP_add.speed +
            armor_SP_add   
    
def SPMsg(env, char):
    p = Packet.Packet()
    p << 'PLAYERSP' << char.SP << MaxSP(env, char)
    return p