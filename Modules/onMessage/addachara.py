from Framework import Message, Auth
from GameData import Class, Character
import Protocol
import Packet

lib = Protocol.Library()

#TODO
@lib.order('addachara')
def addachara(env, user, message):
    if not user.logged:
        print ('user not logged in')
        return

    tag, name, sex, class_, charnum, end = message.split(env.config.SEP)
    sex = int(sex)
    class_ = int(class_)+1
    charnum = int(charnum)
    if not Auth.IsNameValid(name, env.config.PLAYER_CHARSET):
        env.server.send(user, Message.PlainMsg(env.config.PLAYER_CHARSET_ERROR, 4))

    
    elif charnum < 1 or charnum > env.config.MAX_CHARS:
        env.server.send(user, Message.PlainMsg('Tentative de hack', 4)) # TODO

#   TODO:Test Sex 
    elif class_ < 1 or class_ > Class.Class.select().count():
        env.server.send(user, Message.PlainMsg('Erreur interne, veuillez reporter ce que vous avez fait a l\'administrateur du serveur',4))
        # TODO: Hacking attempt
        
# TODO
    elif user.user_entry == None:
        env.server.send(user, Message.PlainMsg('Erreur interne, veuillez reporter ce que vous avez fait a l\'administrateur du serveur',4))
        user.logged = False

    else:
        print('class : %s' % (class_))
        class_type = Class.Class.get(class_)
        sprite = 0
        if sex == 0:
            print ('male')
            sprite = class_type.malesprite
        else:
            print ('female')
            sprite = class_type.femalesprite
            
        char = Character.Character( user = user.user_entry ) # TODO
        char.set( 
            name = name,
            class_type = class_type,
            sex = sex,
            access = 0,
            HP = class_type.maxHP,
            MP = class_type.maxMP,
            SP = class_type.maxSP,
            STR = class_type.STR,
            DEF = class_type.DEF,
            speed = class_type.speed,
            magie = class_type.magie,
            sprite = sprite,
            game_map = env.config.START_MAP,
            x = env.config.START_X,
            y = env.config.START_Y,
            direction = env.config.START_DIRECTION
            )
            
        env.server.send(user, Message.CharListMsg(user.user_entry, env.config))
        env.server.send(user, Message.PlainMsg('Le personnage a ete cree!', 5))
        
