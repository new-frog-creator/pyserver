#-*- encoding:utf-8 -*-
# GATGLASSES    :   envoi la liste des classes et leurs caractéristiques
# INPUT SYNTAX  :   gatglasses
# OUTPUT SYNTAX :   newcharclasses , nombre_de_classes, [name, maxHP, maxMP, maxSP, STR, DEF, speed, magie, malesprite, femalesprite, locked]
# AUTHOR        :   Alexandre Janniaux

from Framework import Message, Auth
import Config
import Protocol
import Packet
from GameData import Class
lib = Protocol.Library()

@lib.order('gatglasses')
def gatglasses(env, user, message):
    nbclass = Class.Class.select().count()
    p = Packet.Packet()
    p << 'NEWCHARCLASSES'
    p << nbclass-1
    
    print ('Ily a %s classes dispos' % (nbclass))
    
    if nbclass >=1:
        for i in range(nbclass):
            mclass      = Class.Class.get(i+1)

            p << mclass.name
            
            p << mclass.maxHP
            p << mclass.maxMP
            p << mclass.maxSP
            
            p << mclass.STR
            p << mclass.DEF
            p << mclass.speed
            p << mclass.magie
            
            p << mclass.malesprite
            p << mclass.femalesprite
            p << int(mclass.locked)
    env.server.send(user, p)
