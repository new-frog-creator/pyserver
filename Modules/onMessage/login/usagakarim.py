"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

from Framework import Message, Auth, Info
from Modules.Player import *
import Config
import Protocol
from GameData import User, Character
import Packet

lib = Protocol.Library()

@lib.order('usagakarim')
def usagakarim(env, user, message):     
    if user.user_entry == None:
        env.server.send(user, Message.PlainMsg('Non connecte', 4)) # TODO : hackingattemp
        return
    char = int(message.split(env.config.SEP)[1])
    print('Le joueur %s utilise le personnage %s' %(user.user_entry.name, char))
   
    chars = Character.Character.selectBy(user=user.user_entry)
    if char < 1 or char > chars.count():
        env.server.send(user, Message.PlainMsg('Numero de message invalide', 4)) # TODO : hackingattemp
        return
    
    p = Packet.Packet()
    p << 'LOGINOK' << user.user_entry.id
    env.server.send(user, p)
    # TODO equiped items
    infos = (
        Info.ClassesMsg(env),
        Info.ItemsMsg(env),
        Info.PetsMsg(env),
        Info.EmoticonsMsg(env),
        Info.ArrowsMsg(env),
        Info.NpcsMsg(env),
        Info.ShopsMsg(env),
        Info.SpellsMsg(env),
        #quest
        #inventory
        #worn equipment
        Stats.HPMsg(env, chars[char]),
        Stats.MPMsg(env, chars[char]),
        Stats.SPMsg(env, chars[char]),
        Stats.Stats(env, chars[char]),
        Info.WeatherMsg(env),
        Info.TimeMsg(env),
        Message.Picvalue,
    )

    for i in infos:
        if i != None:
            env.server.send(user, i)
            pass
    ingame_p = Packet.Packet
    ingame_p << "INGAME"
    env.server.send(ingame_p)
