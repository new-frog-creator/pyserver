# LOGINATION    :   login player into game
# INPUT SYNTAX  :   logination SEP name SEP password SEP version.MAJOR SEP version.MINOR SEP version.REVISION SEP securityKey1 SEP securityKey2 SEP securityKey3 SEP securityKey4 SEP END
# OUTPUT SYNTAX :   Message.MaxInfo()
# AUTHOR        :   Alexandre Janniaux

from Framework import Message, Auth
import Config
import Protocol
from GameData import User
lib = Protocol.Library()

@lib.order('logination')
def logination(env, user, message):     

    try:
        action, name, password, major_version, minor_version, revision, key1, key2, key3, key4, end = message.split(env.config.SEP)
        print '  '.join(message.split(env.config.SEP))
        if user.logged:
            p = Message.PlainMsg('Vous etes deja connecte !',2)
            env.server.send(user, p)

        else:
            if not Auth.IsNameValid(name, env.config.PLAYER_CHARSET):
                p = Message.PlainMsg(env.config.PLAYER_CHARSET_ERROR,2)
                env.server.send(user, p)

##            elif int(major_version) < env.config.MAJOR_VERSION or int(major_version) > env.config.MAJOR_VERSION:
##                p = Message.PlainMsg('Votre client n\'est pas compatible',2)
##                env.server.send(user,p)
##
##            elif int(minor_version) < env.config.MINOR_VERSION:
##                p = Message.PlainMsg('Votre client n\'est pas a jour',2)
##                env.server.send(user, p)
##
##            elif int(minor_version) > env.config.MINOR_VERSION:
##                p = Message.PlainMsg('Le serveur a subit une retrogradation',2)
##                env.server.send(user, p)
            else:
                players = User.User.selectBy(name=name)
                if players.count() == 0:
                    p = Message.PlainMsg('Mauvais mot de passe ou nom de compte',2)
                    env.server.send(user, p)
                else:
                    player = players[0]
                    if not player.password == password:
                        p = Message.PlainMsg('Mauvais mot de passe ou nom de compte',2)
                        env.server.send(user, p)
                    else:
                        user.logged = True
                        user.user_entry = player
                        player.connected = True
                        maxinfo = Message.MaxInfo(env.config)
                        env.server.send(user, maxinfo)
                        print ('%s s\'est connecte avec l\'ip %s !' % (name,user.ip))
                        charlist = Message.CharListMsg(player,env.config)
                        env.server.send(user,charlist)    

    except ValueError as a:
        print ('erreur de valeur')

