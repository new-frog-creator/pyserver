# -*- coding: utf-8 -*-

"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

"""
 NEWFACCOUNTIED :      Create account
 INPUT SYNTAX :        newfaccountied SEP name SEP password
 OUTPUT SYNTAX:
 AUTHOR :              alexandre janniaux
"""

from Framework import Message, Auth
from GameData import User
import Protocol

lib = Protocol.Library()

@lib.order('newfaccountied')
def newfaccountied(env, user, message):
    print ('Création d\'un nouveau compte')
    try:
        name, password = message.split(env.config.SEP)[1:3]
        if User.User.selectBy(name=name).count() != 0:
            packet = Message.PlainMsg('Pseudo deja existant',1)
            env.server.send (user,packet)
            print ('Le nom de compte existait deja.')

        else:
            player = User.User()
            player.name = name
            player.password = password

            p = Message.PlainMsg('Votre compte a ete cree',2)
            env.server.send (user,p)
            print ('Le compte a bien été créé')
    except ValueError:
        print ('Erreur de reception, la création a été annulée')
