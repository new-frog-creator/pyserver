# PICVALUE        :   Send pic values
# INPUT SYNTAX    :   PICVALUE
# OUTPUT SYNTAX   :   PICVALUE, PIC_PL, PIC_NPC1, PIC_NPC2, AccModo, AccMapper
# AUTHOR          :   Alexandre Janniaux

from Framework import Message, Auth
import Config
import Protocol
import Packet

lib = Protocol.Library()

#TODO
@lib.order('picvalue')
def picvalue(env, user, message):
    p = Packet.Packet()
    p << 'PICVALUE' 
    p << env.pic.pl        
    p << env.pic.npc1      
    p << env.pic.npc2      
    p << env.pic.accmodo   
    p << env.pic.accmapper 
    p << env.pic.accdev    
    p << env.pic.accadmin  

    env.server.send(user,p)
