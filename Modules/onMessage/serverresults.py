
"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

"""
 SERVERRESULTS :       Send server information to the client
 INPUT SYNTAX :        serverresults
 RETURN SYNTAX :       serverresults    id    totalNumberOfClients    maxClients
 AUTHOR :              alexandre janniaux
"""

import Config
import Protocol
import Packet
from GameData import User

lib = Protocol.Library()

@lib.order('serverresults')
def  serverresults(env, user, message):
    try:
        sep = env.config.SEP
        end = env.config.END
        serverresults = Packet.Packet(sep, end)
        serverresults << "serverresults" << 0 << User.User.selectBy(connected=True).count() << str(env.config.MAX_PLAYERS) # TODO: count players
        user.socket.sendall(serverresults.buffer())

    except TypeError as e:
        print ('erreur dans serverresults : %s' % e)
