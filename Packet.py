#-*- coding:utf-8 -*-
"""
Facilitation des transferts réseaux
"""

import Config

class Packet(object):
    """
    La classe packet permet de stocker des messages et d'automatiquement rajouter les bons séparateurs
    
    Usage:

    import Packet
    pack = Packet.Packet('5', '0')
    pack << "MESSAGE" << "ARGUMENT 1" << "toi"
    print pack
    >>> MESSAGE5ARGUMENT 15toi50

    print

    pack.sep = ' '
    pack.end = '[FIN]'

    print str(pack)

    >>> MESSAGE ARGUMENT 1 toi [FIN]
    """
    def __init__   ( self, sep=Config.SEP, end=Config.END ):
        self._message = []
        self.sep = str(sep)
        self.end = str(end)

    def __lshift__ ( self, message ):
        self._message.append(str(message))
        return self

    def __str__ ( self ):
        return str(self._message)

    def buffer     ( self ):
        return bytes(str(self.sep).join(self._message) + str(self.sep) + str(self.end))

