# -*- coding: utf-8 -*-

"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

"""
Permet de supporter une fonction d'un protocole


"""

from Decorators import singleton

@singleton
class Library():
    """
    Bibliothèque de commande contenant le protocole

    HOW-TO:

    library = Library() # ne pas oublier l'import
    
    @library.command(nom de l'evenement)
    def mafonction (env, message):

    """
    _orders = {}
    
    def order(self,name):
        """
        Décorateur utilisé pour importer une commande
        """

        def parameter(function):
            
            def make_command():
                if not name in self._orders:
                    self._orders[name] = []
                if not function in self._orders[name]:
                    self._orders[name].append(function)
                    print ('la fonction %s a été ajouté à la commande %s' % (function, name))
            make_command()
            return function
        return parameter

    def getOrders(self):
        return self._orders
