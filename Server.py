# -*- coding: utf-8 -*-
"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

""" 
Module serveur,
gère les différents aspects de la liaison client/serveur
"""

import socket
import threading
import Signal
import Packet

class User(object):
    """
    Représentation dans le réseau d'un client du serveur
    Stocke la relation utilisateur/joueur/administrateur
    """

    logged    = False
    """ Utilisateur connecté au jeu ou non """

    gameID      = -1
    """ Player ID in-game du joueur """

    gameName    = ''
    """ Nom in-game du joueur """

    socket      = None
    """ Socket pour l'envoi de messages """

    ip          = None
    """ Ip de l'utilisateur """

    port        = None
    """ Port de l'utilisateur """

    user_entry  = None
    

class Server(object):
    """
    Implémentation du serveur dans le réseau,
    Récupère et envoit les messages sur le réseau

    onConnexion :   signal activé à la connexion d'un nouveau client
    onDisjunction:  signal activé à la deconnexion d'un client
    onMessage :     signal activé à l'arrivée d'un message par un client
    """
    _host = '127.0.0.1'
    _port = 4000
    _max_co = 3
    _listener = None
    _clients = {}
    _unlogged_clients = []
    _running = False


    def __init__(self,host,port,max_connections=3):
        """
        host :              masque réseau ( '' pour accepter toutes les adresses )
        port :              port réseau
        max_connections :   maximum de connexions simultanées avant l'acceptation de celle ci, 3 est une bonne valeur (cf doc)
        """
        self._host = host
        self._port = port
        self._max_co = max_connections
        self._listener = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

        self.onMessage = Signal.Signal()
        self.onConnexion = Signal.Signal()
        self.onDisjunction = Signal.Signal()

    def __del__(self):
        self._running = False;
        for client in self._unlogged_clients:
            client.socket.close()
        for client in self._clients:
            client.socket.close()
    
        self._listener.close()

    def start(self):
        """
        Demarre l'écoute du serveur
        """
        self._listener.bind((self._host, self._port))
        self._listener.listen(self._max_co)
        self._running = True
# TODO: make sockets thread restart
    def handleSocket(self, user):
        """
        Vérifie l'état des sockets (messages reçus, deconnection ...)
        Pour usage interne seulement
        """
        print ('Handle ip %s ' % user.ip)
        while(self._running):
            try:
                data = user.socket.recv(4096)
                if data == '':
                    self.onDisjunction(user)
                    user.socket.close()
                    del user
                    return
                self.onMessage(user,data);
            except:
                self.onDisjunction(user)
                user.socket.close()
                del user
                return

                                


    def loop(self):
        """
        Actualise le status du serveur (acceptation des connections)
        """
        conn, addr = self._listener.accept()
        user = User()
        user.socket = conn
        user.ip = addr[0]
        user.port = addr[1]
        
        self._unlogged_clients.append(user)

        self.onConnexion(user)
        threading.Thread(target=self.handleSocket, args=(user,)).start()

    def send(self, user, packet):
        if isinstance(packet, Packet.Packet):
            user.socket.sendall(packet.buffer())
            print ('send to %s : %s' % (user.ip, packet.buffer())) 

        elif (isinstance(packet, tuple) or isinstance(packet, list)) and len(packet)>=1:
            for pack in packet:
                send(user, pack)
                    
        else:
            print ('[SERVER] unknown data')
            print (packet)
