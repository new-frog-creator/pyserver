#-*- encoding: utf-8 -*-

from GameData import Class

def AutoCreateClass():
    print ('[AutoCreateClass] Ensure at least one class exists')
    if Class.Class.select().count() == 0:
        print('[AutoCreateClass] No class created, creating new one :')
        name = raw_input('Name:')
        maxHP = int(raw_input('max HP:'))
        maxMP = int(raw_input('max MP:'))
        maxSP = int(raw_input('max SP:'))
        STR = int(raw_input('strength:'))
        DEF = int(raw_input('defense:'))
        speed = int(raw_input('speed:'))
        magie = int(raw_input('magie:'))
        malesprite = int(raw_input('malesprite:'))
        femalesprite = int(raw_input('femalesprite:'))
        locked = bool(raw_input('locked:'))
        
        new_class = Class.Class(
            name = name,
            maxHP = maxHP,
            maxMP = maxMP,
            maxSP = maxSP,
            STR = STR,
            DEF = DEF,
            speed = speed,
            magie = magie,
            malesprite = malesprite,
            femalesprite = femalesprite,
            locked = locked
        )
        
        print ('[AutoCreateClass] Class created')
    else:
        print ('[AutoCreateClass] OK')        

        
