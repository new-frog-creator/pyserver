# -*- coding: utf-8 -*- 

"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

"""
Configuration du serveur
"""


SEP = chr(0)
""" Separateur de caractere dans le serveur
    par défaut, 0
"""

END = chr(237)
""" Signal la fin d'un message dans le serveur
    par défaut, 237
"""


    
PLAYER_CHARSET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
PLAYER_CHARSET_ERROR = 'Le nom ne doit contenir que des lettres et des chiffres'

ADDRESS = ''
PORT    = 22000

GAME_NAME = 'test'
GAME_ID   = 0
"""
Utilisé pour le multiserveur
"""

MAJOR_VERSION = 0
MINOR_VERSION = 6

MAX_CHARS	= 3
MAX_PLAYERS	= 500
MAX_ITEMS	= 100
MAX_NPCS	= 100
MAX_SHOP	= 100
MAX_SPELLS	= 100
MAX_MAPS	= 100
MAX_MAP_ITEMS	= 100
MAX_MAPX	= 100
MAX_MAPY	= 100
MAX_EMOTICONS	= 100
MAX_LEVEL	= 100
MAX_QUETES	= 100
MAX_INV		= 100
MAX_NPC_SPELLS	= 100
MAX_PETS        = 100

START_MAP = 1
START_X   = 1
START_Y   = 1
START_DIRECTION = 0
