# -*- coding: utf-8 -*-
"""
Copyright (C) 2010 JANNIAUX Alexandre

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

"""
Test du module de sauvegarde
"""

import XmlManager
import DataObjects
import os
o = DataObjects.Object()
o.ID = 1
o.name = "danman"
o.data['type'] = 'developpeur'
p = DataObjects.Object()
p.ID = 2
p.name = "clavier noir"
p.data['qualite'] = 'excellent'
c = DataObjects.Category('touches')
c.data['type'] = 'grosses touches'
p.data['mestouches'] = c
o.data['clavier'] = p
m = XmlManager.Manager(os.path.join(os.path.dirname(__file__),'data'))
m.save(o)

print ('\n\nReloading data ...')

f = m.byID(1)
print ('dev n°%s : %s' % (f.ID, f.name))
print ('%s' % f.data)
print ('touches : %s' % f.data['clavier'].data['mestouches'])

